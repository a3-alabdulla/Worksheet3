#include <cstdio>
#include <iostream>
using std::cout;
using std::endl;

int main (){
    for (int i =0; i<31; i++){
        if (i % 3 == 0 && i % 5 == 0){
            printf("fizz bizz \n");
        }
        else if (i % 3 == 0)
        {
            printf("fizz \n");
        }
        else if (i % 5 == 0){
            printf("bizz \n");
        }
        else
        {
            cout << i << "\n";
        }

    } 
    return 0;
}